//import express js
const express = require("express");
//import thư viện path
const path = require ("path")

//khởi tạo app
const app = express();

//khai báo cổng chạy app
const port = 8000;

//call api chạy project pizza 365
app.get("/",(request, response) =>{
    response.sendFile(path.join(__dirname + "/views/Task 43.40.html"))
})
//hiển thị hình ảnh cần thêm  middleware static vào express
app.use(express.static(__dirname +"/views"))

//chạy app trên cổng 
app.listen(port, ()=>{
    console.log(`App is running on port ${port}`)
})
